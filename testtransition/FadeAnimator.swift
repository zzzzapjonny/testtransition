//
//  FadeAnimator.swift
//  testtransition
//
//  Created by Bergstrom Per Jonny on 2018/05/14.
//  Copyright © 2018 Zappallas. All rights reserved.
//

import UIKit

class FadeAnimator: NSObject {
	let duration: TimeInterval = 1.0
	var originFrame = CGRect.zero
}

extension FadeAnimator: UIViewControllerAnimatedTransitioning {
	func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
		return self.duration
	}

	func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

		let containerView = transitionContext.containerView

		let toView = transitionContext.view(forKey: .to)!

		containerView.addSubview(toView)
		toView.alpha = 0

		UIView.animate(withDuration: self.duration, animations: {
			toView.alpha = 1
		}, completion: { _ in
			transitionContext.completeTransition(true)
		})
	}
}
