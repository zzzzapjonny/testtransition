//
//  SubController.swift
//  testtransition
//
//  Created by Bergstrom Per Jonny on 2018/05/14.
//  Copyright © 2018 Zappallas. All rights reserved.
//

import UIKit

class SubController: UIViewController {

	class func instance() -> SubController? {
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		let vc = storyboard.instantiateViewController(withIdentifier: "SubController")
		if let vc = vc as? SubController {
			return vc
		}
		return nil
	}


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

	override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		return .portrait
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		print("touch!")
		super.touchesBegan(touches, with: event)

		self.dismiss(animated: true) {
			//
		}
	}
}
