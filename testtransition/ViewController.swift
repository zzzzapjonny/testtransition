//
//  ViewController.swift
//  testtransition
//
//  Created by Bergstrom Per Jonny on 2018/05/14.
//  Copyright © 2018 Zappallas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	let transition = FadeAnimator.init()

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	@IBAction func actionGosub() {
		guard let vc = SubController.instance() else {
			print("No sub vc!")
			return
		}
		vc.transitioningDelegate = self

		self.present(vc, animated: true) {
			//
		}
	}
}

extension ViewController: UIViewControllerTransitioningDelegate {
	public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		print("animationController")
		return self.transition
	}

	func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		return self.transition
	}
}
